﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearInfoOnClick : MonoBehaviour {

	public void OnMouseUpAsButton () {
		GameManager.instance.infoScreen.transmissionToShow = null;
	}
}
