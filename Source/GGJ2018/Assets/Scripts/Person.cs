﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


// Class: Person
// Purpose: This is a data-only class that holds a person -- we don't ever have people in our GameObjects, so it's not a MonoBehavior 
[CreateAssetMenu (fileName = "PresetPerson", menuName = "FakeNews/Person")]
public class Person : ScriptableObject {

    public enum Gender
    {
        Male,
        Female,
    }

    
	public string firstName;
	public string lastName;
    public Gender gender;
    public bool bIsCelebrity = false;
    public float chanceOfGainingFriend;
    public float timeToNextPost;
    public float postCooldownTime;
	public Sprite portrait; // May change to Sprite -- let's chat - HUE
	public List<TopicSet> opinions;
	[Range(0,1)]public float percentChanceToRepost;
    public int numberOfFriends;
	public List<Person> friendsList;    


	// Function: Make Random
	// Purpose: Makes a random person
	public Person MakeRandom () {

        string firstNamesTemp;
        string lastNamesTemp;
        string[] firstNames;
        string[] lastNames;
        opinions = new List<TopicSet>();

        //Character used to split the names when pulled in from the text files
        char deliminator = '\n';

        timeToNextPost = Time.time;

        float rnd = Random.value;

        if (rnd < 0.5f)
            gender = Gender.Male;
        else if (rnd >= 0.5f)
            gender = Gender.Female;

        rnd = Random.value;
        if(rnd <= GameManager.instance.options.chanceToBeCelebrity)
        {
            bIsCelebrity = true;
        }

        if (bIsCelebrity)
        {
            chanceOfGainingFriend = GameManager.instance.options.celebrityChanceToGainFriend;
            percentChanceToRepost = GameManager.instance.options.chanceToPostCelebrity;
            postCooldownTime = GameManager.instance.options.timeToNextPostCelebrity;
        }
        else
        {
            chanceOfGainingFriend = GameManager.instance.options.nonCelebrityChanceToGainFriend;
            percentChanceToRepost = GameManager.instance.options.chanceToPostNonCelebrity;
            postCooldownTime = GameManager.instance.options.timeToNextPostNonCelebrity;
        }

        lastNamesTemp = GameManager.instance.lastNames.text;
        lastNames = lastNamesTemp.Split(deliminator);

        int rndNum = Random.Range(0, lastNames.Length - 1);

        lastName = lastNames[rndNum];

        if (gender == Gender.Male)
        {
            firstNamesTemp = GameManager.instance.maleFirstNames.text;

            firstNames = firstNamesTemp.Split(deliminator);

            rndNum = Random.Range(0, firstNames.Length - 1);

            firstName = firstNames[rndNum];

            rndNum = Random.Range(0, GameManager.instance.options.maleFaces.Count - 1);

            portrait = GameManager.instance.options.maleFaces[rndNum];

        }
        else if (gender == Gender.Female)
        {
            firstNamesTemp = GameManager.instance.femaleFirstNames.text;

            firstNames = firstNamesTemp.Split(deliminator);

            rndNum = Random.Range(0, firstNames.Length - 1);

            firstName = firstNames[rndNum];

            rndNum = Random.Range(0, GameManager.instance.options.femaleFaces.Count - 1);

            portrait = GameManager.instance.options.femaleFaces[rndNum];
        }

        GenerateOpinions();
        

		// Return this person
		return this;
	}

    public void GenerateOpinions()
    {
        foreach (Topic topic in GameManager.instance.options.topics)
        {
            float rndOpinion = Random.Range(-100, 100);
            int newState = Random.Range(0, GameManager.instance.options.transmissionTypes.Count - 1);
            TransmissionType state = GameManager.instance.options.transmissionTypes[newState];

            TopicSet newOpinion = new TopicSet(topic, rndOpinion, state);

            opinions.Add(newOpinion);
        }
    }

    public void AddFriend(Person friend)
    {
        friendsList.Add(friend);
    }
}  

[System.Serializable]
public struct Post {
	public Person poster;
	public Transmission transmission;
	public bool bIsHacked;
}


[System.Serializable]
public struct TopicSet {
	public Topic topic;
	[Range(-100,100)] public float changeInOpinion;
	public TransmissionType emoState;

    public TopicSet(Topic inTopic, float inOpinion, TransmissionType inState)
    {
        topic = inTopic;
        changeInOpinion = inOpinion;
        emoState = inState;
    }

    public TopicSet(Topic inTopic, float inOpinion)
    {
        topic = inTopic;
        changeInOpinion = inOpinion;
        emoState = null;
    }
}

