﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Class: Quest
// Purpose: This is a single "job" or quest --- get the average score for "topic" to 
[CreateAssetMenu( fileName = "Quest", menuName = "FakeNews/Quest")]
public class Quest : ScriptableObject {
	public string id;
	[TextArea(3,3)]
	public string headline;
	public Topic topic;
	public Vector2 targetRange;
	public float baseReward;

	// Function: GenerateRandom
	// Purpose: Generates a random quest
	public void GenerateRandom () {
		// TODO: Set values so this is a random Quest -- in case we need them
	}

	// TODO: Function that, on creation of object, adjusts the reward based on how hard the quest will be from our current state

}
