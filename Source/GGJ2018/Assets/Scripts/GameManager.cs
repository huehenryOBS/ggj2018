﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour {

	// Singleton
	public static GameManager instance;

	// Elements
	public Options options;
	public List<Person> people;
	public List<Quest> quests;
	public Feed feed;
	public InfoController infoScreen;
	public DiskController diskController;
	public CardManager cardManager;
    public PollsManager pollManager;
	public JobController jobManager;
	public CameraControls cameraManager;
	public float playerScore;


    [Header("Person Names")]
    [Tooltip("Text file holding male names for random generation")]
    public TextAsset maleFirstNames;
    [Tooltip("Text file holding female names for random generation")]
    public TextAsset femaleFirstNames;
    [Tooltip("Text file holding last names for random generation")]
    public TextAsset lastNames;

	[Header("Time")]
	public int minutes;
	public int seconds;
	public string timeDisplay;
    
	void Awake () {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
    }

    private void Start()
    {
        options.globalTimeToNextPost = Time.time;
        if(pollManager == null)
        {
            pollManager = GameObject.FindObjectOfType<PollsManager>();
        }

        LoadResources();
        GeneratePopulation();
        CreateFriendsLists();

        pollManager.GenerateBaseTopics();
        pollManager.StartCoroutine(pollManager.UpdatePollsDisplay());

		// Draw our first hand
		cardManager.fillDeck();

		// Update the disks
		diskController.UpdateDisks();


		// Start the Timer
		StartCoroutine (GameTimer ());


        /*
        string personInfo;
        foreach(Person person in people)
        {
            if(person.bIsCelebrity)
            {
                personInfo = "**CELEBRITY** First Name: " + person.firstName + " - Last Name: " + person.lastName + " - Number of Friends: " + person.friendsList.Count.ToString() + " Chance to post: " + person.percentChanceToRepost.ToString();
                Debug.Log(personInfo);
                string topicInfo;
                foreach(TopicSet topic in person.opinions)
                {
                    topicInfo = "Topic: " + topic.topic.id + " - Current Opinion: " + topic.changeInOpinion.ToString() + " - State: " + topic.emoState.id;
                    Debug.Log(topicInfo);
                }
            }
            else
            {
                personInfo = "First Name: " + person.firstName + " - Last Name: " + person.lastName + " - Number of Friends: " + person.friendsList.Count.ToString() + " Chance to post: " + person.percentChanceToRepost.ToString();
                Debug.Log(personInfo);
                string topicInfo;
                foreach (TopicSet topic in person.opinions)
                {
                    topicInfo = "Topic: " + topic.topic.id + " - Current Opinion: " + topic.changeInOpinion.ToString() + " - State: " + topic.emoState.id;
                    Debug.Log(topicInfo);
                }
            }
        }*/
    }

    // Update is called once per frame
    void Update () {
        Post();
        //Debug.Log("Player Score: " + playerScore.ToString());

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}


	// Funciton: GeneratePopulation
	// Purpose: Make all the people
	public void GeneratePopulation (bool deleteCurrentPeople = false)

    {
		if (deleteCurrentPeople)
			people.Clear ();

		// Note -- if there are already people, don't lose them
		for (int i = people.Count; i < options.numberOfPeople; i++) {
            Person newPerson = ScriptableObject.CreateInstance("Person") as Person;
			newPerson.MakeRandom();
			people.Add (newPerson);
		}
	}


	public void FillQuestList () {
		if (quests == null)
			quests = new List<Quest> ();

		while (quests.Count < jobManager.panels.Count) {
			// TODO: Card deck logic, rather than just a random card
			quests.Add(options.fullQuests[Random.Range(0,options.fullQuests.Count)]);	
		}
	}


    public void CreateFriendsLists()
    {
        for(int i = 0; i < people.Count; i++)
        {
            people[i].friendsList = new List<Person>();
            for(int j = 0; j < people.Count; j++)
            {
                float newFriend = Random.value;
                if( (people[j] != people[i])  && (newFriend <= people[i].chanceOfGainingFriend))
                {
                    people[i].friendsList.Add(people[j]);
                }
            }
        }
    }

	// Function: LoadResources
	// Purpose: LoadResources
	public void LoadResources () {

		TransmissionType[] tempTT = Resources.LoadAll<TransmissionType> ("Data/TransTypes");
		options.transmissionTypes = new List<TransmissionType> (tempTT);

		Quest[] tempQ = Resources.LoadAll<Quest> ("Data/Quests");
		options.fullQuests = new List<Quest> (tempQ);

		Topic[] tempTopic = Resources.LoadAll<Topic> ("Data/Topics");
		options.topics = new List<Topic> (tempTopic);

		Person[] tempP = Resources.LoadAll<Person> ("Data/PresetPeople");
		people = new List<Person> (tempP);

		Transmission[] tempTrans = Resources.LoadAll<Transmission> ("Data/Transmissions");
		options.fullDeck = new List<Transmission> (tempTrans);

        Sprite[] tempMFace = Resources.LoadAll<Sprite>("Data/Faces/MaleFaces");
        options.maleFaces = new List<Sprite> (tempMFace);

        Sprite[] tempFFace = Resources.LoadAll<Sprite>("Data/Faces/FemaleFaces");
        options.femaleFaces = new List<Sprite>(tempFFace);
	}




    public void Post()
    {
        //Debug.Log("Post");
        foreach(Person person in people)
        {
            if(options.globalTimeToNextPost <= Time.time)
            {
                if (person.timeToNextPost <= Time.time)
                {
                    float canPost = Random.value;
                    if (canPost <= person.percentChanceToRepost)
                    {
                        bool bIsPostitive = false;

                        int topicIndex = Random.Range(0, person.opinions.Count - 1);
                        TopicSet newTopic = person.opinions[topicIndex];

                        if (newTopic.changeInOpinion < 0)
                            bIsPostitive = false;
                        else if (newTopic.changeInOpinion > 0)
                            bIsPostitive = true;

                        List<Transmission> posts = new List<Transmission>();

                        if (bIsPostitive)
                        {
                            posts.Clear();
                            posts = (from top in GameManager.instance.options.fullDeck where top.topic == newTopic.topic && top.transmissionType == newTopic.emoState && top.changeInOpinion > 0 select top).ToList<Transmission>();

                        }
                        else
                        {
                            posts.Clear();
                            posts = (from top in GameManager.instance.options.fullDeck where top.topic == newTopic.topic && top.transmissionType == newTopic.emoState && top.changeInOpinion < 0 select top).ToList<Transmission>();
                        }

                        if (posts.Count != 0)
                        {
                            int newPostIndex = Random.Range(0, posts.Count - 1);

                            Post newPost;
                            newPost.poster = person;
                            newPost.transmission = posts[newPostIndex];
							newPost.bIsHacked = false;

                            while (GameManager.instance.feed.posts.Count >= options.maxFeedItems)
                            {
                                GameManager.instance.feed.posts.RemoveAt(0);
                            }
                            GameManager.instance.feed.posts.Add(newPost);


                            ReadPosts(person, newPost, 0);
                            //Debug.Log("FirstName: " + newPost.poster.firstName + " LastName: " + newPost.poster.lastName + " Topic: " + newPost.transmission.topic + " Change: " + newPost.transmission.changeInOpinion.ToString() + " State: " + newPost.transmission.transmissionType.ToString());
                            person.timeToNextPost = Time.time + person.postCooldownTime;
                            options.globalTimeToNextPost = Time.time + options.globalPostDelay;
                        }
                        else
                        {
                            person.timeToNextPost = Time.time + person.postCooldownTime;
                        }
                    }
                    else
                    {
                        person.timeToNextPost = Time.time + person.postCooldownTime;
                    }
                }
            }
        }
    }

    public void HackPerson(Person person, Transmission inTrans)
    {
        Post newPost;
        newPost.poster = person;
        newPost.transmission = inTrans;
		newPost.bIsHacked = true;
        GameManager.instance.feed.posts.Add(newPost);
		ReadPosts (person, newPost, 0);
    }



    public void ReadPosts(Person person, Post inPost, int passDownRank)
    {
        if(passDownRank < options.postPassDownDecay.Length - 1)
        {
            foreach(Person friend in person.friendsList)
            {
                TopicSet topicToModify = new TopicSet();
                for(int i = 0; i < friend.opinions.Count; i++)
                {
                    if(friend.opinions[i].topic == inPost.transmission.topic)
                    {
                        topicToModify = friend.opinions[i];

                        if(topicToModify.emoState.fullIDs.Contains(inPost.transmission.transmissionType))
                        {
                            float modifyAmount = ((GameManager.instance.options.fullEffectModifier * (inPost.transmission.changeInOpinion)) * GameManager.instance.options.postPassDownDecay[passDownRank]);
							topicToModify.changeInOpinion = topicToModify.changeInOpinion + modifyAmount;
							topicToModify.changeInOpinion = Mathf.Clamp(topicToModify.changeInOpinion, -100, 100);
							friend.opinions [i] = topicToModify;
                            ReadPosts(friend, inPost, passDownRank + 1);
						}
                        else if(topicToModify.emoState.nearIDs.Contains(inPost.transmission.transmissionType))
                        {
                            float modifyAmount = ((GameManager.instance.options.nearEffectModifier * (inPost.transmission.changeInOpinion)) * GameManager.instance.options.postPassDownDecay[passDownRank]);
							topicToModify.changeInOpinion = topicToModify.changeInOpinion + modifyAmount;
							topicToModify.changeInOpinion = Mathf.Clamp(topicToModify.changeInOpinion, -100, 100);
							friend.opinions [i] = topicToModify;
							ReadPosts(friend, inPost, passDownRank + 1);
                        }
                        else if(topicToModify.emoState.backfireIDs.Contains(inPost.transmission.transmissionType))
                        {
                            float modifyAmount = ((GameManager.instance.options.backfireEffectModifier * (inPost.transmission.changeInOpinion)) * GameManager.instance.options.postPassDownDecay[passDownRank]);
							topicToModify.changeInOpinion = topicToModify.changeInOpinion + modifyAmount;
							topicToModify.changeInOpinion = Mathf.Clamp(topicToModify.changeInOpinion, -100, 100);
							friend.opinions [i] = topicToModify;
							ReadPosts(friend, inPost, passDownRank + 1);
                        }
                    }
                }
            }
        }
        else
        {
            // Do nothing
        }
    }

	IEnumerator GameTimer()
	{
		while (options.gameRunTime > 0) {
			options.gameRunTime--;
			minutes = (int)options.gameRunTime / 60;
			seconds = (int)options.gameRunTime % 60;
			timeDisplay = minutes.ToString () + ":" + seconds.ToString ();

			yield return new WaitForSeconds (1);
		}

		Application.Quit();
	}

    [System.Serializable]
	public struct Options {
        
        [Header("People information")]
        [Tooltip("How many people in the world")]
		public int numberOfPeople; // How many Person objects to create total
        [Tooltip("How many cards player can hold")]
		public int numberOfCardsInHand;
        [Tooltip("How many active jobs the player can see")]
		public int numberOfJobsAvailable;
		[Tooltip("Changes the number of followers shown, so we can have 'nicer looking' numbers")]
		public float followersModifier;
        [Tooltip("Chance random person can be a celebrity")]
        [Range(0, 1)] public float chanceToBeCelebrity;
        [Tooltip("The chance a celebrity will have a person as a friend")]
        [Range(0, 1)] public float celebrityChanceToGainFriend;
        [Tooltip("The chance a non celebrity will have a person as a friend")]
        [Range(0, 1)] public float nonCelebrityChanceToGainFriend;
        [Header("Posting Information")]
		[Tooltip("The max number of posts in the feed")]
		[HideInInspector] public int maxFeedItems;
        [Tooltip("The chance a celebrity will post to feed")]
        [Range(0, 1)] public float chanceToPostCelebrity;
        [Tooltip("The chance a non celebirty will post to feed")]
        [Range(0 ,1)] public float chanceToPostNonCelebrity;
        [Tooltip("How long celebrity has to wait before next post")]
        [Range (0, 10)] public float timeToNextPostCelebrity;
        [Tooltip("How long a non celebrity has to wait before next post")]
        [Range (0, 10)] public float timeToNextPostNonCelebrity;
        [Tooltip("Delay that prevents all people from posting")]
        [Range(0, 10)] public float globalPostDelay;
        [HideInInspector]
        public float globalTimeToNextPost;
        [Tooltip("How a post will decay as it moves through friends list")]
        public float[] postPassDownDecay;
        [Header("TransmitionType Information")]
        [Range(0, 1)] public float fullEffectModifier;
        [Range(0, 1)] public float nearEffectModifier;
        [Range(-1, 0)] public float backfireEffectModifier;
        [Header("Topics Information")]
        [Tooltip("List of all topics to post about")]
		public List<Topic> topics;
        [Tooltip("List of types of transmissions")]
		public List<TransmissionType> transmissionTypes;
		[Tooltip("The entire deck of transmissions -- will be loaded in")]
		public List<Transmission> fullDeck;
		[Tooltip("The entire deck of quests -- will be loaded in")]
		public List<Quest> fullQuests;
        [Header("Polls Information")]
        [Tooltip("How often polls update in seconds")]
        public int pollUpdateDelay;
        [Header("Face Images")]
        public List<Sprite> maleFaces;
        public List<Sprite> femaleFaces;
		[Header("Time Information")]
		[Tooltip("How long the game will run before end in seconds")]
		public float gameRunTime;
    }
    
}

