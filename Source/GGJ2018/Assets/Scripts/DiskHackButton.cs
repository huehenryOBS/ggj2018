﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class DiskHackButton : MonoBehaviour {

	[HideInInspector]public Disk disk;
	public GameObject particles;
	public UnityEvent OnHack;
	public UnityEvent OnNonHack;

	// Use this for initialization
	void Start () {
		disk = GetComponent<Disk> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnMouseUpAsButton() {

		if (GameManager.instance.infoScreen.personToShow != null) {
			if (disk != null && disk.transmission != null) {
				GameManager.instance.HackPerson(GameManager.instance.infoScreen.personToShow, disk.transmission);
				GameManager.instance.cardManager.hand.Remove (disk.transmission);
				GameManager.instance.cardManager.hand.Add (GameManager.instance.cardManager.DrawCard ());
				OnHack.Invoke ();

				// Play cool particles here
				if (particles != null) {
					Instantiate (particles, transform.position, transform.rotation);
				}
				// TODO: Play sound
				//GameManager.instance.infoScreen.transmissionToShow = disk.transmission;
			}
		} else {
			if (disk != null && disk.transmission != null) {
				GameManager.instance.infoScreen.transmissionToShow = disk.transmission;
				OnNonHack.Invoke ();
			}
		}
	}
}
