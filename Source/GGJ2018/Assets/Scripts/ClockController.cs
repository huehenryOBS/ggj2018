﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ClockController : MonoBehaviour {

	public Text timeBox;

	// Use this for initialization
	void Start () {		
	}
	
	// Update is called once per frame
	void Update () {
		timeBox.text = GameManager.instance.timeDisplay;		
	}
}
