﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiskController : MonoBehaviour {

	public List<Disk> disks;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateDisks (){
		// For each disk
		for (int i=0; i<disks.Count; i++) {
			// If there is a card in our hand at that number
			if ( GameManager.instance.cardManager.hand.Count > i ) {
				// turn it on and set it
				//Debug.Log("CARD "+i+": is "+GameManager.instance.cardManager.hand[i].headline);
				disks[i].gameObject.SetActive(true);
				disks [i].transmission = GameManager.instance.cardManager.hand [i];
				disks [i].UpdateDisplay ();
			} else {
				//Debug.Log("CARD "+i+": is NULL + "+GameManager.instance.cardManager.hand[i].headline);
				// Otherwise, it is null
				disks[i].gameObject.SetActive(false);
				disks [i].transmission = null;
			}
		}
	}
}
