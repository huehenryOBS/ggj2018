﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UIManagerA : MonoBehaviour {

	public static UIManagerA instance;

	[Header("Buttons")]
	public Button start;//starts the game
	public Button quit;//exits game

	public List<Button> btns;

	public GameObject creditsPanel;//holds the panel of the credits

	public List<string> underscores;


	void Awake(){
		if(instance == null){
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else{
			Destroy (this.gameObject);
		}

	}

	void Update(){
		
	}

	//exits game
	public void Quit(){
		Application.Quit ();
	}

	//loads main game scene
	public void StartGame(){
		SceneManager.LoadScene ("Main");//load main game
	}

	//credits stuff
	public void Credits(){
		creditsPanel.SetActive (true);
	}

	public void CloseCredits(){
		creditsPanel.SetActive (false);
	}

	public void hideUnderline(int btnNo) {
		//Debug.Log (btnNo);
		btns [btnNo].transform.GetComponentInChildren<Text> ().text = "";
	}

	public void showUnderline(int btnNo) {
		
		btns [btnNo].transform.GetComponentInChildren<Text> ().text = underscores[btnNo];
	}
}