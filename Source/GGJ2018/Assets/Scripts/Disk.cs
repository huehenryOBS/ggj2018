﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Disk : MonoBehaviour {

	private Transmission _transmission;
	public Transmission transmission { 
		get { return _transmission; }
		set { _transmission = value;
			UpdateDisplay ();
			}
	}
	[Header("UI Elements")]
	public Image TopicImage;
	public Image TypeImage;
	public Image posImage;
	public Image negImage;
	public Text headlineText;
	public Text summaryText;


	// Use this for initialization
	void Start () {
		// Start with images off
		posImage.gameObject.SetActive (false);
		negImage.gameObject.SetActive (false);
		TopicImage.sprite = null;
		TypeImage.sprite = null;
		headlineText.text = "";
		summaryText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateDisplay () {
		headlineText.text = "";
		summaryText.text = "";
		posImage.gameObject.SetActive (false);
		negImage.gameObject.SetActive (false);

		if (_transmission != null) {
			TopicImage.sprite = _transmission.topic.icon;
			TypeImage.sprite = _transmission.transmissionType.icon;
			if (_transmission.changeInOpinion > 0) {
				posImage.gameObject.SetActive (true);
			} else {
				negImage.gameObject.SetActive (true);
			}
			headlineText.text = _transmission.headline;
			summaryText.text = _transmission.summary;
		}
	}
}
