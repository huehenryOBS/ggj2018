﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "Topic", menuName = "FakeNews/Topic")]
public class Topic : ScriptableObject {
	public string id;
	public string displayName;
	public Sprite icon;
}
