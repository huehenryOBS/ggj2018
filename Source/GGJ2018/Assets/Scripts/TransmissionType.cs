﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class: Transmission Type
// Purpose: In our brainstorm, this was "Happy / Sad / Excited / Calm " -- players match these to change the amount of effect. 

[CreateAssetMenu( fileName = "Transmission", menuName = "FakeNews/TransmissionType")]
public class TransmissionType : ScriptableObject {

	public string id;  // Transmissions with same type id get 100% value
	public Sprite icon; // Icon to show  (Note, may move this to a sprite -- depends on how we want to work -- let's chat! - HUE )
	public List<TransmissionType> fullIDs; // Transmissions with any of the IDs in this list get full (100%) effect on opinion
	public List<TransmissionType> nearIDs; // Transmissions with any of the IDs in this list get a small (defined in GameManager.settings) effect on opinion
	public List<TransmissionType> backfireIDs; // Transmissions with any of the IDs in this list get a an inverted (amount defined in GameManager.settings) effect on opinion
}
