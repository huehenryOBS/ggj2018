﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PollsManager : MonoBehaviour {

	// NOTE: THIS IS GARBAGE! THIS IS WHAAT HAPPENS ON THE LAST DAY OF A JAM -- I NEED TO MAKE THIS EXPANSABLE, NOT HARDCODED TO THESE 3 POLLS!

	[Header("UIElements")]
	public Text topic1Text;
	public Text topic2Text;
	public Text topic3Text;
	public Text topic1NegNum;
	public Text topic2NegNum;
	public Text topic3NegNum;
	public Text topic1PosNum;
	public Text topic2PosNum;
	public Text topic3PosNum;
	public Image topic1Pos;
	public Image topic2Pos;
	public Image topic3Pos;
	public Image topic1Neg;
	public Image topic2Neg;
	public Image topic3Neg;

	public Text cashText;

	// Data
	private List<PollData> averageOpinions;

	// Use this for initialization
	void Start () {
        //Debug.Log("PollsManager start");
        averageOpinions = new List<PollData>();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateDisplay ();
	}

	public void UpdateDisplay(){

		// Quick and Dirty
		cashText.text = "Bank Balance: $"+GameManager.instance.playerScore;

		topic1Text.text = averageOpinions [0].topicSet.topic.displayName;
		topic2Text.text = averageOpinions [1].topicSet.topic.displayName;
		topic3Text.text = averageOpinions [2].topicSet.topic.displayName;

		if (averageOpinions [0].topicSet.changeInOpinion < 0) {
			topic1NegNum.text = ""+Mathf.Floor(averageOpinions [0].topicSet.changeInOpinion);
			topic1PosNum.text = "";
			topic1Pos.gameObject.SetActive (false);
			topic1Neg.gameObject.SetActive (true);
			topic1Neg.fillAmount = averageOpinions[0].topicSet.changeInOpinion / -100.0f;
		} else {
			topic1NegNum.text = "";
			topic1PosNum.text = ""+Mathf.Floor(averageOpinions [0].topicSet.changeInOpinion);
			topic1Neg.gameObject.SetActive (false);
			topic1Pos.gameObject.SetActive (true);
			topic1Pos.fillAmount = averageOpinions[0].topicSet.changeInOpinion / 100.0f;
		}

		if (averageOpinions [1].topicSet.changeInOpinion < 0) {
			topic2NegNum.text = ""+Mathf.Floor(averageOpinions [1].topicSet.changeInOpinion);
			topic2PosNum.text = "";
			topic2Pos.gameObject.SetActive (false);
			topic2Neg.gameObject.SetActive (true);
			topic2Neg.fillAmount = averageOpinions[1].topicSet.changeInOpinion / -100.0f;
		} else {
			topic2NegNum.text = "";
			topic2PosNum.text = ""+Mathf.Floor(averageOpinions [1].topicSet.changeInOpinion);
			topic2Neg.gameObject.SetActive (false);
			topic2Pos.gameObject.SetActive (true);
			topic2Pos.fillAmount = averageOpinions[1].topicSet.changeInOpinion / 100.0f;
		}

		if (averageOpinions [2].topicSet.changeInOpinion < 0) {
			topic3NegNum.text = ""+Mathf.Floor(averageOpinions [2].topicSet.changeInOpinion);
			topic3PosNum.text = "";
			topic3Pos.gameObject.SetActive (false);
			topic3Neg.gameObject.SetActive (true);
			topic3Neg.fillAmount = averageOpinions[2].topicSet.changeInOpinion / -100.0f;
		} else {
			topic3NegNum.text = "";
			topic3PosNum.text = ""+Mathf.Floor(averageOpinions [2].topicSet.changeInOpinion);
			topic3Neg.gameObject.SetActive (false);
			topic3Pos.gameObject.SetActive (true);
			topic3Pos.fillAmount = averageOpinions[2].topicSet.changeInOpinion / 100.0f;
		}




	}

    //Creates the list of topicsets all set to 0
    public void GenerateBaseTopics()
    {
        foreach(Topic top in GameManager.instance.options.topics)
        {
            TopicSet newTopic = new TopicSet(top, 0);
            PollData newData = new PollData(newTopic);
            averageOpinions.Add(newData);
        }
    }

    public IEnumerator UpdatePollsDisplay()
    {
        while(true)
        {
            //Debug.Log("InUpdateDisplay");

            //Resets average number and transmission type counters
            for (int i = 0; i < averageOpinions.Count; i++)
            {
                PollData tempData = averageOpinions[i];
                tempData.topicSet.changeInOpinion = 0;
                tempData.transmissionTypeCounter = 0;
                averageOpinions[i] = tempData;
            }

            //Adds all the opinion for each topic/transmission type combinations
            for (int p = 0; p < GameManager.instance.people.Count; p++)
            {
                Person person = GameManager.instance.people[p];

                for (int o = 0; o < person.opinions.Count; o++)
                {
                    TopicSet opinion = person.opinions[o];

                    for (int a = 0; a < averageOpinions.Count; a++)
                    {
                        PollData data = averageOpinions[a];

                        if (opinion.topic == data.topicSet.topic)
                        {
                            data.transmissionTypeCounter++;
                            data.topicSet.changeInOpinion += opinion.changeInOpinion;
                            averageOpinions[a] = data;
                        }
                    }
                }
            }

            //Averages the data to how many of each transmission types
            for (int i = 0; i < averageOpinions.Count; i++)
            {
                PollData data = averageOpinions[i];
                data.topicSet.changeInOpinion = (data.topicSet.changeInOpinion / data.transmissionTypeCounter);
                averageOpinions[i] = data;
            }

            /*
            string display;
            foreach (PollData data in averageOpinions)
            {
                display = "Topic: " + data.topicSet.topic.id + " Average Opinion: " + data.topicSet.changeInOpinion.ToString() + " State Counter: " + data.transmissionTypeCounter.ToString();
                Debug.Log(display);
            }*/

            CheckQuestCompletion();

            yield return new WaitForSeconds(GameManager.instance.options.pollUpdateDelay);
        }
        
    }

    void CheckQuestCompletion()
    {
        int questIndex = 0;
        bool bQuestCompleted = false;
        for(int i = 0; i < GameManager.instance.quests.Count; i++)
        {
            Quest tempQuest = GameManager.instance.quests[i];
            for(int p = 0; p < averageOpinions.Count; p++)
            {
                PollData data = averageOpinions[p];
                if(data.topicSet.topic == tempQuest.topic)
                {
                    if(data.topicSet.changeInOpinion >= tempQuest.targetRange.x && data.topicSet.changeInOpinion <= tempQuest.targetRange.y)
                    {
                        GameManager.instance.playerScore += tempQuest.baseReward;
                        bQuestCompleted = true;
                        questIndex = i;
                        break;
                    }
                }
            }
        }

        if(bQuestCompleted)
        {
            GameManager.instance.quests.RemoveAt(questIndex);
        }
    }
}

[System.Serializable]
public struct PollData
{
    public TopicSet topicSet;
    public int transmissionTypeCounter;

    public PollData(TopicSet inTopSet)
    {
        topicSet = inTopSet;
        transmissionTypeCounter = 0;
    }

}
