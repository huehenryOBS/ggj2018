﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobController : MonoBehaviour {

	public List<JobPanel> panels;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		UpdateAllJobs ();
		// TODO: TEMP: FOR NOW, JUST KEEP THEM FULL AT ALL TIMES
		KeepQuestsFull ();
	}

	public void KeepQuestsFull() {
		GameManager.instance.FillQuestList ();
	}

	public void UpdateAllJobs () {

		for (int i = 0; i < panels.Count; i++) {

			if (GameManager.instance.quests.Count > i) {
				panels [i].gameObject.SetActive (true);
				panels [i].job = GameManager.instance.quests [i];
			} else {
				panels [i].gameObject.SetActive (false);
			}
		}
	}
}
