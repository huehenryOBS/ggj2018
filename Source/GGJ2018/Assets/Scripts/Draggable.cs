﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour {
	
	private Transform tf;//transform of PARENT NOT THIS GAMEOBJECT
	private bool dragged;
	private Vector3 screenPoint;
	public float offsetOfRay;
	public GameObject explosion;


	// Use this for initialization
	void Start () {
		tf = transform.parent.GetComponent<Transform> ();//transform of  PARENT OBJECT
		dragged=false; //starts false
	}
	
	// Update is called once per frame
	void Update () {
		//checks if this object is being clicked on
		if (dragged) {
			tf.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenPoint.z)); //drag with mouse
			tf.up = (tf.position - Camera.main.transform.position);
			tf.Rotate (0, 180, 0);
		}
			
	}


	//used to make sure it starts following the mouse.
	void OnMouseDown() {
		screenPoint = Camera.main.WorldToScreenPoint (tf.position);//stores the z-axis of the card being draged 
		dragged = true;//confirms the card to start dragging

	}

	//detects the monitor behind the disk with a ray
	void DetectMonitor(){
		RaycastHit hit;//detects if something gets hit
		//draw a ray to detect a monitor behind the disk
		//Debug.DrawRay(tf.position - tf.forward * offsetOfRay, -tf.forward, Color.red);
		//cast a ray behind the disk
		if (Physics.Raycast(tf.position - tf.forward * offsetOfRay, -tf.forward, out hit, Mathf.Infinity)) {
			//if there is an object behind it
			if (hit.collider.gameObject.CompareTag("Monitor")){
				Instantiate (explosion,tf);//instantiate the particle effect (which will destroy the disk after.
			}
		}

	}

	//mouse let go of diskket
	void OnMouseUp() {
		dragged = false; //lets go of disk
		DetectMonitor ();//checks for a monitor behind disk


	}

	//On destruction of disk
	void OnDestroy(){
		//TODO: send message monitor to post stuff



	}

}
