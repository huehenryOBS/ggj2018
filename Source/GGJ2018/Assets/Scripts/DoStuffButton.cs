﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoStuffButton : MonoBehaviour {

	public UnityEvent stuffToDo;

	public void OnMouseUpAsButton(){
		stuffToDo.Invoke ();
	}
}
