﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour {


	public List<Material> letters;//the particles that the system will change.

	ParticleSystemRenderer theSystem;//the particle system we want changing characters from.

	// Use this for initialization
	void Start () {
		theSystem = this.gameObject.GetComponent<ParticleSystemRenderer>(); //stores the particle system renderer
	}
	
	// Update is called once per frame
	void Update () {
		int randNum = Random.Range (0, letters.Count);//take a random number
		theSystem.material = letters [randNum]; //change the particle to the random one.
	}
}
