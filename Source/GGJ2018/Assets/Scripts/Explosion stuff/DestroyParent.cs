﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParent : MonoBehaviour {

	void OnDestroy(){
		Destroy (transform.parent.gameObject);//when it is done playing, it destroys the parent object, as well.
	}
}
