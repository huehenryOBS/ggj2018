﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// TODO: "A person is typing" when it is waiting for a feed to show up



// Class: Feed
// Purpose: This controls and holds data for our SocialMedia feed.

public class Feed : MonoBehaviour {

	public FeedController feedController;
	public List<Post> posts;

	private void Awake()
    {
		feedController = GetComponent<FeedController> ();
        posts = new List<Post>();
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
