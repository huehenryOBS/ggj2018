﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedController : MonoBehaviour {

	public List<FeedPanel> panels;

	// Use this for initialization
	void Start () {
		GameManager.instance.options.maxFeedItems = panels.Count;
	}
	
	// Update is called once per frame
	void Update () {
		UpdateDisplay ();
	}


	// Function: UpdateDisplay
	// Purpose:Updates the feed display -- so we can do this only when we need it
	public void UpdateDisplay() {

		// Start with everything off
		foreach (FeedPanel panel in panels) {
			panel.gameObject.SetActive (false);
		}

		// Instead of using actual feed, make a copy and reverse it
		List<Post> temp = new List<Post>(GameManager.instance.feed.posts);
		temp.Reverse ();

		// if we have things in the feed
		if (temp.Count > 0) {
			// loop through the panels
			for (int i = 0; i<panels.Count; i++) {

				// If no feed for this panel -- just continue to the next panel
				if (i >= temp.Count) {
					continue;
				}

				// Turn on a panel
				panels[i].gameObject.SetActive(true);

				// Set its data
				panels[i].poster = temp[i].poster;

				panels[i].nameText.text = temp[i].poster.firstName + " " + temp[i].poster.lastName;

				if (temp [i].bIsHacked) {
					panels [i].nameText.color = Color.red;
					if (temp [i].bIsHacked) {
						panels [i].nameText.text += " (HACK: ";
						if (temp [i].transmission.changeInOpinion < 0) {
							// Do nothing -- neg sign happens automatically
						} else {
							panels [i].nameText.text += "+";
						}
						panels [i].nameText.text += ""+temp [i].transmission.changeInOpinion;
						panels [i].nameText.text += " to "+temp [i].transmission.topic.displayName;
						panels [i].nameText.text += ")  ";
					}
				} else {
					panels [i].nameText.color = Color.blue;
				}

				panels [i].profilePicture.sprite = temp [i].poster.portrait;
				panels [i].headlineText.text= temp [i].transmission.headline;
				panels [i].summaryText.text = temp [i].transmission.summary;
				panels[i].followerText.text = "" + Mathf.Floor(temp[i].poster.friendsList.Count * GameManager.instance.options.followersModifier) + " friends.";
				if (temp[i].poster.bIsCelebrity) {
					panels [i].followerText.text += " ** CELIBRITY ** ";
				}
			}
		}
	}

}

