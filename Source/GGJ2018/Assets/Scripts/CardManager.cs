﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Class: CardManager
// Purpose: Holds our deck and functions so we can get our objects
public class CardManager : MonoBehaviour {

	// public List<Transmission> fullDeck; // ?? Moved to GameManager! ??
	public List<Transmission> currentDeck;
	public List<Transmission> hand;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GameManager.instance.diskController.UpdateDisks();
	}

	public void fillDeck () {
		while (hand.Count < GameManager.instance.diskController.disks.Count) {
			hand.Add (DrawCard ());
		}
		GameManager.instance.diskController.UpdateDisks();
	}

	// Function: DrawCard
	// Purpose: Draws a random card and removes it from the currentDeck
	//          Drawing a random card is the same shuffling and drawing top card
	public Transmission DrawCard () {
		if (currentDeck.Count <= 0) {
			Reshuffle ();
		}

		if (currentDeck.Count > 0) {
			int randomNumber = Random.Range (0, currentDeck.Count);
			Transmission cardDrawn = currentDeck [randomNumber];
			currentDeck.RemoveAt (randomNumber);
			return cardDrawn;
		} 
		return null;
	}

	// Function: Reshuffle
	// Purpose: Reloads the deck.
	public void Reshuffle () {
		currentDeck.Clear ();
		foreach (Transmission card in GameManager.instance.options.fullDeck) {
			currentDeck.Add (card);		
		}
	}
}
