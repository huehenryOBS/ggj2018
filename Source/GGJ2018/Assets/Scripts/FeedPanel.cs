﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedPanel : MonoBehaviour {

	[Header("UI Elements")]
	public Text nameText;
	public Text headlineText;
	public Text summaryText;
	public Text followerText;
	public Image profilePicture;
	[Header("Data")]
	public Person poster;

	public void SendToInfo () {
		if (poster != null) {
			GameManager.instance.infoScreen.personToShow = poster;
		}
	}

}
