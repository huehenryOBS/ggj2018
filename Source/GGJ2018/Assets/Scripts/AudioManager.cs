﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	// Singleton
	public static AudioManager instance;
	// Sounds
	public AudioClip cardPickup;
	public AudioClip cardDrop;
	public AudioClip cardDropFail;
	// etc. 

	void Awake () {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
