﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JobPanel : MonoBehaviour {

	private Quest _job;
	public Quest job { 
		get { 
			return _job;
		} 
		set {
			_job = value;
			UpdateDisplay ();
		} 
	}

	public Image topicImage;
	public Image posImage;
	public Image negImage;

	public Text valueRangeText;
	public Text moneyText;
	public Text topicText;
	public Text headlineText;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void UpdateDisplay () {
		// Start empty
		posImage.gameObject.SetActive (false);
		negImage.gameObject.SetActive (false);
		valueRangeText.text = "";
		moneyText.text = "";
		topicText.text = "";
		headlineText.text = "";

		if (_job != null) {

			topicImage.sprite = _job.topic.icon;
			if (_job.targetRange.x >= 0 && _job.targetRange.y >= 0) {
				posImage.gameObject.SetActive (true);
			} else if (_job.targetRange.x <= 0 && _job.targetRange.y <= 0) {
				negImage.gameObject.SetActive (true);
			}

			// TODO: Word better for 0's
			valueRangeText.text = _job.targetRange.x + " to " + _job.targetRange.y;

			// TODO: Modify based on how hard it is
			moneyText.text = "$" + _job.baseReward;

			headlineText.text = job.headline;

			topicText.text = _job.topic.displayName;
		}
	}
}
