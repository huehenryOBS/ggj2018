﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoController : MonoBehaviour {

	[Header("UI Elements")]
	public Text nameText;
	public Image profilePicture;
	public Text followersText;
	public Text celebrityText;
	public Text HeadlineText;
	public Text SummaryText;

	public float yForCharts;

	public GameObject canvas;
	public List<GameObject> panels;

	[Header("Prefabs")]
	public GameObject topicPanel;



	// [Header("Values")]
	public Person personToShow { 
		set {
			_transmissionToShow = null;
			_personToShow = value;
			UpdateDisplay ();
		}
		get {
			return _personToShow;
		}
	}
	public Transmission transmissionToShow { 
		set {
			_personToShow = null;
			_transmissionToShow = value;
			UpdateDisplay ();
		}
		get {
			return _transmissionToShow;
		}
	}

	private Person _personToShow; 
	private Transmission _transmissionToShow;


	// Use this for initialization
	void Awake () {
	}

	void ClearPanels () {
		foreach (GameObject panel in panels) {
			Destroy (panel);
		}
		panels.Clear ();
	}

	void Start () {
		// Start clear
		canvas.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
				
	}

	public void UpdateDisplay () {		
		if (_personToShow != null) {
			// Activate the canvas
			canvas.SetActive (true);

			// Set Profile Picture
			profilePicture.sprite = _personToShow.portrait;

			// Set text
			nameText.text = _personToShow.firstName + " " + _personToShow.lastName;
			followersText.text = Mathf.Floor(_personToShow.friendsList.Count * GameManager.instance.options.followersModifier) + " friends.";
			if (_personToShow.bIsCelebrity) {
				celebrityText.text = "** Celebrity **";
			} else {
				celebrityText.text = "";
			}

			// Clear Panels
			ClearPanels();

			// Set Topics
			for (int i=0; i< _personToShow.opinions.Count; i++) {
				// Create a new bar
				GameObject newPanel = Instantiate(topicPanel, canvas.transform) as GameObject;
				RectTransform rTf = newPanel.GetComponent<RectTransform> ();
				rTf.anchoredPosition = new Vector2 (0, yForCharts - (i * 125));
				InfoPanel ip = newPanel.GetComponent<InfoPanel> ();
				ip.topicText.text = _personToShow.opinions [i].topic.displayName;
				ip.typeImage.sprite = _personToShow.opinions [i].emoState.icon;
				if (_personToShow.opinions [i].changeInOpinion > 0) {
					ip.negImage.gameObject.SetActive (false);
					ip.posImage.gameObject.SetActive (true);
					ip.posImage.fillAmount = _personToShow.opinions [i].changeInOpinion / 100.0f;
					ip.posText.text = "" + _personToShow.opinions [i].changeInOpinion;
				} else {
					ip.negImage.gameObject.SetActive (true);
					ip.posImage.gameObject.SetActive (false);
					ip.negImage.fillAmount = _personToShow.opinions [i].changeInOpinion / -100.0f;
					ip.negText.text = "" + _personToShow.opinions [i].changeInOpinion;
				}

				panels.Add (newPanel);
			}

			// Turn off headline and summary text
			HeadlineText.text = "";
			SummaryText.text = "";

		} else if (_transmissionToShow != null) {
			// Turn on headline and summary text
			HeadlineText.gameObject.SetActive(true);
			SummaryText.gameObject.SetActive(true);

			// Clear Panels
			ClearPanels();

			// Activate the canvas
			canvas.SetActive (true);

			// Set Profile Picture
			profilePicture.sprite = _transmissionToShow.topic.icon;

			// Set Headline
			nameText.text = "";
			followersText.text = "";
			celebrityText.text = "";

			// TODO: Headline text
			HeadlineText.text = _transmissionToShow.headline;
			// TODO: Summary Text
			SummaryText.text = _transmissionToShow.summary;
			// TODO: One Panel for how it affects the topic

			// Create a new bar
			GameObject newPanel = Instantiate(topicPanel, canvas.transform) as GameObject;
			RectTransform rTf = newPanel.GetComponent<RectTransform> ();
			rTf.anchoredPosition = new Vector2 (0, yForCharts - 125);
			InfoPanel ip = newPanel.GetComponent<InfoPanel> ();
			ip.topicText.text = _transmissionToShow.topic.displayName;
			ip.typeImage.sprite = _transmissionToShow.transmissionType.icon;
			if (_transmissionToShow.changeInOpinion > 0) {
				ip.negImage.gameObject.SetActive (false);
				ip.posImage.gameObject.SetActive (true);
				ip.posImage.fillAmount = _transmissionToShow.changeInOpinion / 100.0f;
				ip.posText.text = "" + _transmissionToShow.changeInOpinion;
			} else {
				ip.negImage.gameObject.SetActive (true);
				ip.posImage.gameObject.SetActive (false);
				ip.negImage.fillAmount = _transmissionToShow.changeInOpinion / -100.0f;
				ip.negText.text = "" + _transmissionToShow.changeInOpinion;
			}
			panels.Add (newPanel);


		} else {
			canvas.SetActive (false);
		}



	}
}
