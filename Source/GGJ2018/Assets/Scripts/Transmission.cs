﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class: Transmission
// Purpose: These are the "cards" we play --  

[CreateAssetMenu( fileName = "Transmission", menuName = "FakeNews/Transmission")]
public class Transmission : ScriptableObject {
	public string id;
	[TextArea(3,3)]
	public string headline;
	[TextArea(3,3)]
	public string summary;
	public Sprite image;
	public Topic topic;
	[Range(-25,25)] public float changeInOpinion;
	public TransmissionType transmissionType;
}
