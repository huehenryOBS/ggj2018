﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEMPLookAtMe : MonoBehaviour {

	public bool bDontLookAtMe;
	private Quaternion rootRotation;
	//private Transform rootPosition;

	// Use this for initialization
	void Start () {
		//rootPosition = Camera.main.transform.position;
		rootRotation = Camera.main.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnMouseUpAsButton() {
		if (!bDontLookAtMe) {
			Camera.main.transform.LookAt (transform);
		} else {
			Camera.main.transform.rotation = rootRotation;
		}
	}
}
