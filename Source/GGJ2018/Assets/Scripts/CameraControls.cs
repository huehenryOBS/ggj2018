﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraControls : MonoBehaviour {

	public CinemachineBrain brain;
	public CinemachineVirtualCamera MainCamera;
	public CinemachineVirtualCamera FeedCamera;
	public CinemachineVirtualCamera InfoCamera;
	public CinemachineVirtualCamera PollCamera;
	public CinemachineVirtualCamera JobCamera;
	public CinemachineVirtualCamera DiskCamera;

	// Use this for initialization
	public void Start () {
		MainCamera.gameObject.SetActive(true);
		FeedCamera.gameObject.SetActive(false);
		InfoCamera.gameObject.SetActive(false);
		PollCamera.gameObject.SetActive(false);
		JobCamera.gameObject.SetActive(false);
		DiskCamera.gameObject.SetActive(true);
	}


	public void GoToWideShot() {
		MainCamera.gameObject.SetActive(true);
		FeedCamera.gameObject.SetActive(false);
		InfoCamera.gameObject.SetActive(false);
		PollCamera.gameObject.SetActive(false);
		JobCamera.gameObject.SetActive(false);
		DiskCamera.gameObject.SetActive(false);
	}

	public void GoToDiskShot() {
		MainCamera.gameObject.SetActive(false);
		FeedCamera.gameObject.SetActive(false);
		InfoCamera.gameObject.SetActive(false);
		PollCamera.gameObject.SetActive(false);
		JobCamera.gameObject.SetActive(false);
		DiskCamera.gameObject.SetActive(true);
	}

	public void GoToFeedShot () {
		MainCamera.gameObject.SetActive(false);
		FeedCamera.gameObject.SetActive(true);
		InfoCamera.gameObject.SetActive(false);
		PollCamera.gameObject.SetActive(false);
		JobCamera.gameObject.SetActive(false);
		DiskCamera.gameObject.SetActive(false);
	}

	public void GoToInfoShot () {
		MainCamera.gameObject.SetActive(false);
		FeedCamera.gameObject.SetActive(false);
		InfoCamera.gameObject.SetActive(true);
		PollCamera.gameObject.SetActive(false);
		JobCamera.gameObject.SetActive(false);
		DiskCamera.gameObject.SetActive(false);
	}

	public void GoToPollShot() {
		MainCamera.gameObject.SetActive(false);
		FeedCamera.gameObject.SetActive(false);
		InfoCamera.gameObject.SetActive(false);
		PollCamera.gameObject.SetActive(true);
		JobCamera.gameObject.SetActive(false);
		DiskCamera.gameObject.SetActive(false);
	}

	public void GoToJobShot() {
		MainCamera.gameObject.SetActive(false);
		FeedCamera.gameObject.SetActive(false);
		InfoCamera.gameObject.SetActive(false);
		PollCamera.gameObject.SetActive(false);
		JobCamera.gameObject.SetActive(true);
		DiskCamera.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			GoToWideShot ();
		}

		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			GoToFeedShot ();
		}

		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			GoToInfoShot ();
		}

		if (Input.GetKeyDown(KeyCode.Alpha4)) {
			GoToPollShot ();
		}

		if (Input.GetKeyDown(KeyCode.Alpha5)) {
			GoToJobShot ();
		}
		if (Input.GetKeyDown(KeyCode.Alpha6)) {
			GoToDiskShot ();
		}
	}

	public void DebugStuff () {
		Debug.Log ("WORKING!");
	}
}
